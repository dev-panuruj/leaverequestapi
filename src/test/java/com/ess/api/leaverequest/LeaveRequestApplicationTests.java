package com.ess.api.leaverequest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LeaveRequestApplicationTests {

    @Test
    public void contextLoads() {
    }


    @Test
    public void convertTime() {

        int day = 1;
        int hour = day * 8;
        int min = hour * 60;

        //System.out.println(day + " Day : " + min + " Min");

        //leave 2 hour
        int total = 4800;
        int leaveRequest = 120;
        int remain = total - leaveRequest;

        //min to day
        //System.out.println("remain = " + remain);
        int remainDay = remain / 480;
        int remainHour = remain % 480;
        int remainMin = remainHour % 60;

        StringBuilder sb = new StringBuilder();
        sb.append("Day : " + remainDay);
        sb.append(" Time : 0" + remainHour / 60 + ":" + remainMin);

        if (remainMin == 0) {
            sb.append("0");
        }
        //min to hour
        // System.out.println("Total 10 Day, Leave 2 hour, Remain = " + sb.toString());
    }

    @Test
    public void buildAttrachFileDocName() {

        // document name refer by => Company_Code+Emp_ID+Date+Leave_Request_Running_Number+Seq_Number
        DateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
        Date cdate = new Date();
        String docName[] = {"TISCO", "EMP99", dateFormat.format(cdate), "LR01", "1"};

        StringBuilder documentName = new StringBuilder();

        for (int i = 0; i < docName.length; i++) {

            String item = docName[i].toString();

            if (documentName.length() <= 0) {
                documentName.append(item);
            } else {
                documentName.append("_");
                documentName.append(item);
            }
        }

        System.out.println("Attrach file name = " + documentName);
    }

    @Test
    public void buildRunningCode() {

        // 8 digit , year , leave type code
        DateFormat dateFormat = new SimpleDateFormat("yyyy");
        Date cdate = new Date();
        String leaveTypeCode = "LV";
        int runningDigit = 8;
        String startRunningCode = "345678";
        int currentDigit = startRunningCode.length();
        int newRunningCode = Integer.parseInt(startRunningCode) + 1;

        StringBuilder sb = new StringBuilder();

        if (runningDigit == currentDigit) {
            sb.append(newRunningCode);
            sb.append(dateFormat.format(cdate));
            sb.append(leaveTypeCode);
        } else {
            for (int i = 1; i <= runningDigit - currentDigit; i++) {

                if (i == runningDigit - currentDigit) {
                    sb.append(0);
                    sb.append(newRunningCode);
                    sb.append(dateFormat.format(cdate));
                    sb.append(leaveTypeCode);
                } else {
                    sb.append(0);
                }
            }
        }


        System.out.println("running code = " + sb);
    }
}

