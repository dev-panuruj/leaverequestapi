package com.ess.api.leaverequest.service;

import com.ess.api.leaverequest.model.*;
import com.ess.api.leaverequest.requestBody.*;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class LeaveRequestService {

    private DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

    private List<LeaveSummaryDetail> leaveSummaryList;
    private Map<String, LeaveSummaryDetail> leaveSummaryMapping;

    public LastLogin getLastLogin(LeaveRequest request) {

        LastLogin userLastLogin = new LastLogin();
        Date dateNow = new Date();

        userLastLogin.setFullname("Panuruj Bantao");
        userLastLogin.setLastLogin(dateFormat.format(dateNow));

        return userLastLogin;
    }

    public UserInfo getUserInfo(LeaveRequest request) {

        UserInfo userInfo = new UserInfo();
        userInfo.setEmployeeId("tisco5678");
        userInfo.setEmployeeName("ZNiponI");
        userInfo.setScanType("No Scan");
        userInfo.setCostCenter("150000");
        userInfo.setPhoneNumber("02-123-4567");
        userInfo.setEmail("user@tisco.com");
        userInfo.setLocation("Tisco Bank");
        userInfo.setLineId("@Tisco");

        return userInfo;
    }

    public UserLevel getUserLevel(UserLevelRequest request) {

        UserLevel userLevel = new UserLevel();
        userLevel.setLevel1("Function");
        userLevel.setLevel2("Section");
        userLevel.setLevel3("Units");
        userLevel.setLevel4("");
        userLevel.setLevel5("");
        userLevel.setLevel6("");
        userLevel.setLevel7("");
        userLevel.setLevel8("");
        userLevel.setLevel9("");
        userLevel.setLevel10("");

        return userLevel;
    }

    public List<LeaveType> getLeaveType(LeaveTypeRequest request) {

        List<LeaveType> leaveTypeList = new ArrayList<>();

        LeaveType item1 = new LeaveType();
        item1.setLeaveTypeName("Vacation");
        item1.setLeaveTypeValue("vacation");
        item1.setLeaveTypeQuota(10);
        item1.setRemark(false);

        LeaveType item2 = new LeaveType();
        item2.setLeaveTypeName("Sick Leave");
        item2.setLeaveTypeValue("sickLeave");
        item2.setLeaveTypeQuota(30);
        item2.setRemark(true);

        LeaveType item3 = new LeaveType();
        item3.setLeaveTypeName("Maternity Leave");
        item3.setLeaveTypeValue("maternityLeave");
        item3.setLeaveTypeQuota(5);
        item3.setRemark(true);

        LeaveType item4 = new LeaveType();
        item4.setLeaveTypeName("Monkhood Leave");
        item4.setLeaveTypeValue("monkhood");
        item4.setLeaveTypeQuota(15);
        item4.setRemark(true);

        LeaveType item5 = new LeaveType();
        item5.setLeaveTypeName("Military Service Leave");
        item5.setLeaveTypeValue("military");
        item5.setLeaveTypeQuota(180);
        item5.setRemark(true);

        LeaveType item6 = new LeaveType();
        item6.setLeaveTypeName("Leaves Without Pay");
        item6.setLeaveTypeValue("withoutPay");
        item6.setLeaveTypeQuota(3);
        item6.setRemark(true);

        LeaveType item7 = new LeaveType();
        item7.setLeaveTypeName("Company's Business Leave");
        item7.setLeaveTypeValue("businessLeave");
        item7.setLeaveTypeQuota(5);
        item7.setRemark(true);

        leaveTypeList.add(item1);
        leaveTypeList.add(item2);
        leaveTypeList.add(item3);
        leaveTypeList.add(item4);
        leaveTypeList.add(item5);
        leaveTypeList.add(item6);
        leaveTypeList.add(item7);

        return leaveTypeList;
    }

    public List<LeaveReason> getReason(LeaveReasonRequest request) {

        List<LeaveReason> reasonList = new ArrayList<>();
        String LeaveType = request.getLeaveType();

        switch (LeaveType) {
            case "vacation":


                LeaveReason item1 = new LeaveReason();
                item1.setReasonName("Travel Around The World");
                item1.setReasonValue("travel");

                LeaveReason item2 = new LeaveReason();
                item2.setReasonName("Travel Natural");
                item2.setReasonValue("travel");

                reasonList.add(item1);
                reasonList.add(item2);

                break;

            case "sickLeave":

                LeaveReason item3 = new LeaveReason();
                item3.setReasonName("Headache");
                item3.setReasonValue("headache");

                LeaveReason item4 = new LeaveReason();
                item4.setReasonName("High Tempreture");
                item4.setReasonValue("high tempreture");

                reasonList.add(item3);
                reasonList.add(item4);

                break;

            case "maternityLeave":

                LeaveReason item5 = new LeaveReason();
                item5.setReasonName("Abort");
                item5.setReasonValue("abort");

                LeaveReason item6 = new LeaveReason();
                item6.setReasonName("Sudden");
                item6.setReasonValue("sudden");

                reasonList.add(item5);
                reasonList.add(item6);

                break;

            case "monkhood":

                LeaveReason item7 = new LeaveReason();
                item7.setReasonName("1 Rainy Season");
                item7.setReasonValue("1rainySeason");

                LeaveReason item8 = new LeaveReason();
                item8.setReasonName("2 Rainy Season");
                item8.setReasonValue("2rainySeason");

                reasonList.add(item7);
                reasonList.add(item8);

                break;

            case "military":

                LeaveReason item9 = new LeaveReason();
                item9.setReasonName("1 Year Military");
                item9.setReasonValue("1 year");

                LeaveReason item10 = new LeaveReason();
                item10.setReasonName("6 Month Military");
                item10.setReasonValue("6 month");

                reasonList.add(item9);
                reasonList.add(item10);

                break;

            case "withoutPay":

                LeaveReason item11 = new LeaveReason();
                item11.setReasonName("Reason withoutPay 1");
                item11.setReasonValue("withoutPay1");

                LeaveReason item12 = new LeaveReason();
                item12.setReasonName("Reason withoutPay 2");
                item12.setReasonValue("withoutPay2");

                reasonList.add(item11);
                reasonList.add(item12);

                break;

            case "businessLeave":

                LeaveReason item13 = new LeaveReason();
                item13.setReasonName("Reason businessLeave 1");
                item13.setReasonValue("businessLeave1");

                LeaveReason item14 = new LeaveReason();
                item14.setReasonName("Reason businessLeave 1");
                item14.setReasonValue("businessLeave2");

                reasonList.add(item13);
                reasonList.add(item14);

                break;
        }

        return reasonList;
    }

    public LeaveSummary getLeaveSummary(LeaveSummaryRequest request) {

        LeaveSummary leaveSummary = new LeaveSummary();
        leaveSummaryList = new ArrayList<>();

        String lastLeaveCode = "";
        String leaveTypeCode = "LV";
        String runningNo = generateRunningCode(lastLeaveCode, leaveTypeCode);

        // provide data
        LeaveSummaryDetail type1 = new LeaveSummaryDetail();
        type1.setLeaveTypeName("Vacation");
        type1.setLeaveTypeValue("vacation");
        type1.setUsedDay(0);
        type1.setUsedTime("00:00");
        type1.setInProcessApproveDay(0);
        type1.setInProcessApproveTime("00:00");
        type1.setInProcessCancelDay(0);
        type1.setInProcessCancelTime("00:00");
        type1.setTotalDay(10);
        type1.setTotalTime("00:00");
        type1.setRemainingDay(10);
        type1.setRemainingTime("00:00");

        // provide data
        LeaveSummaryDetail type2 = new LeaveSummaryDetail();
        type2.setLeaveTypeName("SickLeave");
        type2.setLeaveTypeValue("sickLeave");
        type2.setUsedDay(0);
        type2.setUsedTime("00:00");
        type2.setInProcessApproveDay(0);
        type2.setInProcessApproveTime("00:00");
        type2.setInProcessCancelDay(0);
        type2.setInProcessCancelTime("00:00");
        type2.setTotalDay(30);
        type2.setTotalTime("00:00");
        type2.setRemainingDay(30);
        type2.setRemainingTime("00:00");

        // provide data
        LeaveSummaryDetail type3 = new LeaveSummaryDetail();
        type3.setLeaveTypeName("Maternity Leave (normal)");
        type3.setLeaveTypeValue("maternity(normal)");
        type3.setUsedDay(0);
        type3.setUsedTime("00:00");
        type3.setInProcessApproveDay(0);
        type3.setInProcessApproveTime("00:00");
        type3.setInProcessCancelDay(0);
        type3.setInProcessCancelTime("00:00");
        type3.setTotalDay(15);
        type3.setTotalTime("00:00");
        type3.setRemainingDay(15);
        type3.setRemainingTime("00:00");

        // provide data
        LeaveSummaryDetail type4 = new LeaveSummaryDetail();
        type4.setLeaveTypeName("Maternity Leave (abnormal)");
        type4.setLeaveTypeValue("maternity(abnormal)");
        type4.setUsedDay(0);
        type4.setUsedTime("00:00");
        type4.setInProcessApproveDay(0);
        type4.setInProcessApproveTime("00:00");
        type4.setInProcessCancelDay(0);
        type4.setInProcessCancelTime("00:00");
        type4.setTotalDay(15);
        type4.setTotalTime("00:00");
        type4.setRemainingDay(15);
        type4.setRemainingTime("00:00");

        // provide data
        LeaveSummaryDetail type5 = new LeaveSummaryDetail();
        type5.setLeaveTypeName("Monkhood Leave");
        type5.setLeaveTypeValue("monkhood");
        type5.setUsedDay(0);
        type5.setUsedTime("00:00");
        type5.setInProcessApproveDay(0);
        type5.setInProcessApproveTime("00:00");
        type5.setInProcessCancelDay(0);
        type5.setInProcessCancelTime("00:00");
        type5.setTotalDay(60);
        type5.setTotalTime("00:00");
        type5.setRemainingDay(60);
        type5.setRemainingTime("00:00");

        // provide data
        LeaveSummaryDetail type6 = new LeaveSummaryDetail();
        type6.setLeaveTypeName("Military Service Leave");
        type6.setLeaveTypeValue("military");
        type6.setUsedDay(0);
        type6.setUsedTime("00:00");
        type6.setInProcessApproveDay(0);
        type6.setInProcessApproveTime("00:00");
        type6.setInProcessCancelDay(0);
        type6.setInProcessCancelTime("00:00");
        type6.setTotalDay(180);
        type6.setTotalTime("00:00");
        type6.setRemainingDay(180);
        type6.setRemainingTime("00:00");

        // provide data
        LeaveSummaryDetail type7 = new LeaveSummaryDetail();
        type7.setLeaveTypeName("Leave Without Pay");
        type7.setLeaveTypeValue("withoutpay");
        type7.setUsedDay(0);
        type7.setUsedTime("00:00");
        type7.setInProcessApproveDay(0);
        type7.setInProcessApproveTime("00:00");
        type7.setInProcessCancelDay(0);
        type7.setInProcessCancelTime("00:00");
        type7.setTotalDay(5);
        type7.setTotalTime("00:00");
        type7.setRemainingDay(5);
        type7.setRemainingTime("00:00");

        // provide data
        LeaveSummaryDetail type8 = new LeaveSummaryDetail();
        type8.setLeaveTypeName("Company's Business Leave");
        type8.setLeaveTypeValue("company");
        type8.setUsedDay(0);
        type8.setUsedTime("00:00");
        type8.setInProcessApproveDay(0);
        type8.setInProcessApproveTime("00:00");
        type8.setInProcessCancelDay(0);
        type8.setInProcessCancelTime("00:00");
        type8.setTotalDay(20);
        type8.setTotalTime("00:00");
        type8.setRemainingDay(20);
        type8.setRemainingTime("00:00");

        // provide data
        LeaveSummaryDetail type9 = new LeaveSummaryDetail();
        type9.setLeaveTypeName("Personal's Business Leave");
        type9.setLeaveTypeValue("personal");
        type9.setUsedDay(0);
        type9.setUsedTime("00:00");
        type9.setInProcessApproveDay(0);
        type9.setInProcessApproveTime("00:00");
        type9.setInProcessCancelDay(0);
        type9.setInProcessCancelTime("00:00");
        type9.setTotalDay(12);
        type9.setTotalTime("00:00");
        type9.setRemainingDay(12);
        type9.setRemainingTime("00:00");

        // provide data
        LeaveSummaryDetail type10 = new LeaveSummaryDetail();
        type10.setLeaveTypeName("Leave for Sterilization");
        type10.setLeaveTypeValue("sterilization");
        type10.setUsedDay(0);
        type10.setUsedTime("00:00");
        type10.setInProcessApproveDay(0);
        type10.setInProcessApproveTime("00:00");
        type10.setInProcessCancelDay(0);
        type10.setInProcessCancelTime("00:00");
        type10.setTotalDay(5);
        type10.setTotalTime("00:00");
        type10.setRemainingDay(5);
        type10.setRemainingTime("00:00");

        // provide data
        LeaveSummaryDetail type11 = new LeaveSummaryDetail();
        type11.setLeaveTypeName("Leave for Self Development");
        type11.setLeaveTypeValue("selfdevelop");
        type11.setUsedDay(0);
        type11.setUsedTime("00:00");
        type11.setInProcessApproveDay(0);
        type11.setInProcessApproveTime("00:00");
        type11.setInProcessCancelDay(0);
        type11.setInProcessCancelTime("00:00");
        type11.setTotalDay(15);
        type11.setTotalTime("00:00");
        type11.setRemainingDay(15);
        type11.setRemainingTime("00:00");

        // provide data
        LeaveSummaryDetail type12 = new LeaveSummaryDetail();
        type12.setLeaveTypeName("Other..");
        type12.setLeaveTypeValue("other");
        type12.setUsedDay(0);
        type12.setUsedTime("00:00");
        type12.setInProcessApproveDay(0);
        type12.setInProcessApproveTime("00:00");
        type12.setInProcessCancelDay(0);
        type12.setInProcessCancelTime("00:00");
        type12.setTotalDay(5);
        type12.setTotalTime("00:00");
        type12.setRemainingDay(5);
        type12.setRemainingTime("00:00");

        //add to list
        leaveSummaryList.add(type1);
        leaveSummaryList.add(type2);
        leaveSummaryList.add(type3);
        leaveSummaryList.add(type4);
        leaveSummaryList.add(type5);
        leaveSummaryList.add(type6);
        leaveSummaryList.add(type7);
        leaveSummaryList.add(type8);
        leaveSummaryList.add(type9);
        leaveSummaryList.add(type10);
        leaveSummaryList.add(type11);
        leaveSummaryList.add(type12);
        //leaveSummaryList.add(type13);

        // map data
        leaveSummaryMapping = new HashMap<>();

        for (LeaveSummaryDetail list : leaveSummaryList) {
            leaveSummaryMapping.put(list.getLeaveTypeValue(), list);
        }

        //generate code leave request

        leaveSummary.setLeaveRequestCode(runningNo);
        leaveSummary.setLeaveSummaryTable(leaveSummaryList);

        return leaveSummary;
    }

    public LeaveSummary getLeaveSummaryDay(LeaveSummaryDayRequest request) {

        LeaveSummary leaveSummary = new LeaveSummary();
        leaveSummaryList = new ArrayList<>();

        String leaveType = request.getLeaveType();
        String leaveTypeCode = "LV";
        String lastLeaveCode = request.getLastLeaveRequest();
        String runningNo = generateRunningCode(lastLeaveCode, leaveTypeCode);
        Integer day = request.getLeaveDay();

        LeaveSummaryDetail item = new LeaveSummaryDetail();
        item = leaveSummaryMapping.get(leaveType);

        if (day < item.getRemainingDay()) {
            int remainingDay = item.getRemainingDay() - day;
            leaveSummaryMapping.get(leaveType).setUsedDay(day);
            leaveSummaryMapping.get(leaveType).setRemainingDay(remainingDay);
        }

//        Map<String, LeaveSummaryDetail> result = leaveSummaryMapping.entrySet().stream()
//                .sorted(Map.Entry.comparingByKey())
//                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
//                        (oldValue, newValue) -> oldValue, LinkedHashMap::new));
//
        leaveSummaryMapping.forEach((k, v) -> {
            leaveSummaryList.add(v);
        });
//
//        leaveSummaryMapping = result;

        //collect email list and sending
        //leaveSummaryList.add(leaveSummaryMapping.get(leaveType));
        leaveSummary.setLeaveRequestCode(runningNo);
        leaveSummary.setLeaveSummaryTable(leaveSummaryList);

        return leaveSummary;
    }

    public LeaveSummary getLeaveSummaryTime(LeaveSummaryTimeRequest request) {

        LeaveSummary leaveSummary = new LeaveSummary();
        leaveSummaryList = new ArrayList<>();

        String leaveType = request.getLeaveType();
        String leaveTypeCode = "LV";
        String lastLeaveCode = request.getLastLeaveRequest();
        String runningNo = generateRunningCode(lastLeaveCode, leaveTypeCode);
        Integer hour = request.getLeaveHour();
        Integer minute = request.getLeaveMinute();
        Integer leaveMinute = hourToMinute(hour) + minute;

        LeaveSummaryDetail item = new LeaveSummaryDetail();
        item = leaveSummaryMapping.get(leaveType);

        Integer totalMinute = dayToMinute(item.getRemainingDay()) + timeToMinute(item.getRemainingTime());
        Integer remainingMinute = totalMinute - leaveMinute;
        Integer remainingDay = minuteToDay(remainingMinute);
        String remainingTime = hourMinuteBuild(remainingMinute);
        String usedTime = hourMinuteBuild(leaveMinute);
        Integer usedDay = item.getRemainingDay() - remainingDay;

        if (leaveMinute < remainingMinute) {
            leaveSummaryMapping.get(leaveType).setUsedDay(usedDay);
            leaveSummaryMapping.get(leaveType).setUsedTime(usedTime);
            leaveSummaryMapping.get(leaveType).setRemainingDay(remainingDay);
            leaveSummaryMapping.get(leaveType).setRemainingTime(remainingTime);
        }

//        Map<String, LeaveSummaryDetail> result = leaveSummaryMapping.entrySet().stream()
//                .sorted(Map.Entry.comparingByKey())
//                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
//                        (oldValue, newValue) -> oldValue, LinkedHashMap::new));
//
        leaveSummaryMapping.forEach((k, v) -> {
            leaveSummaryList.add(v);
        });

//        leaveSummaryMapping = result;

        //generate code leave request
        //lastLeave = lastLeave + 1;
        //leaveSummaryList.add(leaveSummaryMapping.get(leaveType));
        leaveSummary.setLeaveRequestCode(runningNo);
        leaveSummary.setLeaveSummaryTable(leaveSummaryList);

        return leaveSummary;
    }

    private Integer hourToMinute(int hour) {
        int minuteInHour = 60;
        return hour * minuteInHour;
    }

    private Integer dayToMinute(int day) {
        int hour = day * 8;
        return hourToMinute(hour);
    }

    private Integer minuteToHour(int minute) {
        int minuteInHour = 60;
        int dayInMinute = minuteInHour * 8;
        return (minute % dayInMinute) / minuteInHour;
    }

    private Integer minuteToDay(int minute) {
        int minuteInHour = 60;
        int hourInDay = 8;
        int minuteInday = minuteInHour * hourInDay;
        return minute / minuteInday;
    }

    private Integer minuteToMinute(int minute) {
        int minuteInHour = 60;
        return minute % minuteInHour;
    }

    private Integer timeToMinute(String time) {

        int timeTominute = 0;
        int hour = 0;
        int minute = 0;
        String[] timeList = time.split(":");

        if (!timeList[0].equals("00")) {
            hour = Integer.parseInt(timeList[0].replaceAll("0", ""));
        }

        if (!timeList[1].equals("00")) {
            minute = Integer.parseInt(timeList[1].replaceAll("0", ""));
        }

        timeTominute = hourToMinute(hour) + minute;
        return timeTominute;
    }

    private String hourMinuteBuild(int minute) {

        int remainHour = minuteToHour(minute);
        int remainMin = minuteToMinute(minute);

        StringBuilder sb = new StringBuilder();
        sb.append(0);
        sb.append(remainHour);
        sb.append(":");
        sb.append(remainMin);

        if (remainMin == 0) {
            sb.append("0");
        }

        return sb.toString();
    }

    private String generateDocumentName() {

        // document name refer by => Company_Code+Emp_ID+Date+Leave_Request_Running_Number+Seq_Number
        DateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
        Date cdate = new Date();

        List<String> itemDoc = new ArrayList<>();
        itemDoc.add("TISCO");
        itemDoc.add("EMP99");
        itemDoc.add(dateFormat.format(cdate));
        itemDoc.add("LR01");
        itemDoc.add("1");

        StringBuilder documentName = new StringBuilder();

        for (String item : itemDoc) {

            if (documentName.length() == 0) {
                documentName.append(item);
            } else {
                documentName.append("_");
                documentName.append(item);
            }
        }

        return documentName.toString();
    }

    private String generateRunningCode(String leaveRequestCode, String leaveTypeCode) {

        Integer digits = 8;
        DateFormat dateFormat = new SimpleDateFormat("yyyy");
        Date currentDate = new Date();
        String lastRequestCode;

        if (leaveRequestCode.equals("")) {
            lastRequestCode = "0";
        } else {
            lastRequestCode = leaveRequestCode.substring(0, 8);
        }

        Integer currentDigit = lastRequestCode.replaceAll("0", "").length();
        Integer newRequestCode = Integer.parseInt(lastRequestCode) + 1;

        StringBuilder runningCode = new StringBuilder();

        if (digits.equals(currentDigit)) {
            runningCode.append(newRequestCode);
            runningCode.append(dateFormat.format(currentDate));
            runningCode.append(leaveTypeCode);
        } else {
            for (int i = 1; i <= digits - currentDigit; i++) {

                if (i == (digits - currentDigit)) {

                    if (currentDigit.equals(0)) {
                        runningCode.append(newRequestCode);
                        runningCode.append(dateFormat.format(currentDate));
                        runningCode.append(leaveTypeCode);
                    } else {
                        runningCode.append(0);
                        runningCode.append(newRequestCode);
                        runningCode.append(dateFormat.format(currentDate));
                        runningCode.append(leaveTypeCode);
                    }

                } else {
                    runningCode.append(0);
                }
            }
        }

        return runningCode.toString();
    }
}
