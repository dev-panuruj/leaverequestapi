package com.ess.api.leaverequest.controller;

import com.ess.api.leaverequest.model.*;
import com.ess.api.leaverequest.requestBody.*;
import com.ess.api.leaverequest.service.LeaveRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(value = "/api")
@ComponentScan(value = "com.ess.api.leaverequest.service")
public class LeaveRequestController {


    @Autowired
    LeaveRequestService service;

    @PostMapping(value = "/lastLogin")
    public LastLogin lastLogin(@RequestBody LeaveRequest request) {
        return service.getLastLogin(request);
    }

    @PostMapping(value = "/userInfo")
    public UserInfo userInfo(@RequestBody LeaveRequest request) {
        return service.getUserInfo(request);
    }

    @PostMapping(value = "/userLevel")
    public UserLevel userlevel(@RequestBody UserLevelRequest request) {
        return service.getUserLevel(request);
    }

    @PostMapping(value = "/leaveType")
    public List<LeaveType> leaveTypes(@RequestBody LeaveTypeRequest request) {
        return service.getLeaveType(request);
    }

    @PostMapping(value = "/leaveReasons")
    public List<LeaveReason> leaveReasons(@RequestBody LeaveReasonRequest request) {
        return service.getReason(request);
    }

    @PostMapping(value = "/leaveSummary")
    public LeaveSummary leaveSummary(@RequestBody LeaveSummaryRequest request) {
        return service.getLeaveSummary(request);
    }

    @PostMapping(value = "/leaveSummary/day")
    public LeaveSummary leaveSummaryDay(@RequestBody LeaveSummaryDayRequest request) {
        return service.getLeaveSummaryDay(request);
    }

    @PostMapping(value = "/leaveSummary/hour")
    public LeaveSummary leaveSummaryHour(@RequestBody LeaveSummaryTimeRequest request) {
        return service.getLeaveSummaryTime(request);
    }
}
