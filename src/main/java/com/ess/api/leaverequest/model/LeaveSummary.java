package com.ess.api.leaverequest.model;

import java.util.List;

public class LeaveSummary {


    private String leaveRequestCode;
    private List<LeaveSummaryDetail> leaveSummaryTable;

    public LeaveSummary() {
        super();
    }

    public String getLeaveRequestCode() {
        return leaveRequestCode;
    }

    public void setLeaveRequestCode(String leaveRequestCode) {
        this.leaveRequestCode = leaveRequestCode;
    }

    public List<LeaveSummaryDetail> getLeaveSummaryTable() {
        return leaveSummaryTable;
    }

    public void setLeaveSummaryTable(List<LeaveSummaryDetail> leaveSummaryTable) {
        this.leaveSummaryTable = leaveSummaryTable;
    }

    @Override
    public String toString() {
        return "LeaveSummary{" +
                "leaveRequestCode='" + leaveRequestCode + '\'' +
                ", leaveSummaryTable=" + leaveSummaryTable +
                '}';
    }
}
