package com.ess.api.leaverequest.model;

public class LeaveType {

    private String leaveTypeName;
    private String leaveTypeValue;
    private Integer leaveTypeQuota;
    private Boolean remark;

    public LeaveType() {
        super();
    }

    public String getLeaveTypeName() {
        return leaveTypeName;
    }

    public void setLeaveTypeName(String leaveTypeName) {
        this.leaveTypeName = leaveTypeName;
    }

    public String getLeaveTypeValue() {
        return leaveTypeValue;
    }

    public void setLeaveTypeValue(String leaveTypeValue) {
        this.leaveTypeValue = leaveTypeValue;
    }

    public Integer getLeaveTypeQuota() {
        return leaveTypeQuota;
    }

    public void setLeaveTypeQuota(Integer leaveTypeQuota) {
        this.leaveTypeQuota = leaveTypeQuota;
    }

    public Boolean getRemark() {
        return remark;
    }

    public void setRemark(Boolean remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "LeaveType{" +
                "leaveTypeName='" + leaveTypeName + '\'' +
                ", leaveTypeValue='" + leaveTypeValue + '\'' +
                ", leaveTypeQuota=" + leaveTypeQuota +
                ", remark=" + remark +
                '}';
    }
}
