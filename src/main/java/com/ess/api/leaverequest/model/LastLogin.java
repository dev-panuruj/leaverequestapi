package com.ess.api.leaverequest.model;

public class LastLogin {

    private String fullname;
    private String lastLogin;


    public LastLogin() {
        super();
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(String lastLogin) {
        this.lastLogin = lastLogin;
    }

    @Override
    public String toString() {
        return "LastLogin{" +
                "fullname='" + fullname + '\'' +
                ", lastLogin='" + lastLogin + '\'' +
                '}';
    }
}
