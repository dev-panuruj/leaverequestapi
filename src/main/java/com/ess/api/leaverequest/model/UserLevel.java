package com.ess.api.leaverequest.model;

public class UserLevel {

    private String Level1;
    private String Level2;
    private String Level3;
    private String Level4;
    private String Level5;
    private String Level6;
    private String Level7;
    private String Level8;
    private String Level9;
    private String Level10;

    public UserLevel() {
        super();
    }

    public String getLevel1() {
        return Level1;
    }

    public void setLevel1(String level1) {
        Level1 = level1;
    }

    public String getLevel2() {
        return Level2;
    }

    public void setLevel2(String level2) {
        Level2 = level2;
    }

    public String getLevel3() {
        return Level3;
    }

    public void setLevel3(String level3) {
        Level3 = level3;
    }

    public String getLevel4() {
        return Level4;
    }

    public void setLevel4(String level4) {
        Level4 = level4;
    }

    public String getLevel5() {
        return Level5;
    }

    public void setLevel5(String level5) {
        Level5 = level5;
    }

    public String getLevel6() {
        return Level6;
    }

    public void setLevel6(String level6) {
        Level6 = level6;
    }

    public String getLevel7() {
        return Level7;
    }

    public void setLevel7(String level7) {
        Level7 = level7;
    }

    public String getLevel8() {
        return Level8;
    }

    public void setLevel8(String level8) {
        Level8 = level8;
    }

    public String getLevel9() {
        return Level9;
    }

    public void setLevel9(String level9) {
        Level9 = level9;
    }

    public String getLevel10() {
        return Level10;
    }

    public void setLevel10(String level10) {
        Level10 = level10;
    }

    @Override
    public String toString() {
        return "UserLevel{" +
                "Level1='" + Level1 + '\'' +
                ", Level2='" + Level2 + '\'' +
                ", Level3='" + Level3 + '\'' +
                ", Level4='" + Level4 + '\'' +
                ", Level5='" + Level5 + '\'' +
                ", Level6='" + Level6 + '\'' +
                ", Level7='" + Level7 + '\'' +
                ", Level8='" + Level8 + '\'' +
                ", Level9='" + Level9 + '\'' +
                ", Level10='" + Level10 + '\'' +
                '}';
    }
}
