package com.ess.api.leaverequest.model;

public class LeaveSummaryDetail {

    private String leaveTypeName;
    private String leaveTypeValue;
    private Integer usedDay;
    private String usedTime;
    private Integer inProcessApproveDay;
    private String inProcessApproveTime;
    private Integer inProcessCancelDay;
    private String inProcessCancelTime;
    private Integer totalDay;
    private String totalTime;
    private Integer remainingDay;
    private String remainingTime;

    public LeaveSummaryDetail() {
        super();
    }

    public String getLeaveTypeName() {
        return leaveTypeName;
    }

    public void setLeaveTypeName(String leaveTypeName) {
        this.leaveTypeName = leaveTypeName;
    }

    public String getLeaveTypeValue() {
        return leaveTypeValue;
    }

    public void setLeaveTypeValue(String leaveTypeValue) {
        this.leaveTypeValue = leaveTypeValue;
    }

    public Integer getUsedDay() {
        return usedDay;
    }

    public void setUsedDay(Integer usedDay) {
        this.usedDay = usedDay;
    }

    public String getUsedTime() {
        return usedTime;
    }

    public void setUsedTime(String usedTime) {
        this.usedTime = usedTime;
    }

    public Integer getInProcessApproveDay() {
        return inProcessApproveDay;
    }

    public void setInProcessApproveDay(Integer inProcessApproveDay) {
        this.inProcessApproveDay = inProcessApproveDay;
    }

    public String getInProcessApproveTime() {
        return inProcessApproveTime;
    }

    public void setInProcessApproveTime(String inProcessApproveTime) {
        this.inProcessApproveTime = inProcessApproveTime;
    }

    public Integer getInProcessCancelDay() {
        return inProcessCancelDay;
    }

    public void setInProcessCancelDay(Integer inProcessCancelDay) {
        this.inProcessCancelDay = inProcessCancelDay;
    }

    public String getInProcessCancelTime() {
        return inProcessCancelTime;
    }

    public void setInProcessCancelTime(String inProcessCancelTime) {
        this.inProcessCancelTime = inProcessCancelTime;
    }

    public Integer getTotalDay() {
        return totalDay;
    }

    public void setTotalDay(Integer totalDay) {
        this.totalDay = totalDay;
    }

    public String getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(String totalTime) {
        this.totalTime = totalTime;
    }

    public Integer getRemainingDay() {
        return remainingDay;
    }

    public void setRemainingDay(Integer remainingDay) {
        this.remainingDay = remainingDay;
    }

    public String getRemainingTime() {
        return remainingTime;
    }

    public void setRemainingTime(String remainingTime) {
        this.remainingTime = remainingTime;
    }

    @Override
    public String toString() {
        return "LeaveSummaryDetail{" +
                "leaveTypeName='" + leaveTypeName + '\'' +
                ", leaveTypeValue='" + leaveTypeValue + '\'' +
                ", usedDay=" + usedDay +
                ", usedTime='" + usedTime + '\'' +
                ", inProcessApproveDay=" + inProcessApproveDay +
                ", inProcessApproveTime='" + inProcessApproveTime + '\'' +
                ", inProcessCancelDay=" + inProcessCancelDay +
                ", inProcessCancelTime='" + inProcessCancelTime + '\'' +
                ", totalDay=" + totalDay +
                ", totalTime='" + totalTime + '\'' +
                ", remainingDay=" + remainingDay +
                ", remainingTime='" + remainingTime + '\'' +
                '}';
    }
}
