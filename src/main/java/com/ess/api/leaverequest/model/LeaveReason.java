package com.ess.api.leaverequest.model;

public class LeaveReason {

    private String reasonName;
    private String reasonValue;

    public LeaveReason() {
        super();
    }

    public String getReasonName() {
        return reasonName;
    }

    public void setReasonName(String reasonName) {
        this.reasonName = reasonName;
    }

    public String getReasonValue() {
        return reasonValue;
    }

    public void setReasonValue(String reasonValue) {
        this.reasonValue = reasonValue;
    }

    @Override
    public String toString() {
        return "LeaveReason{" +
                "reasonName='" + reasonName + '\'' +
                ", reasonValue='" + reasonValue + '\'' +
                '}';
    }
}
