package com.ess.api.leaverequest.requestBody;

public class LeaveTypeRequest {

    private String userId;

    public LeaveTypeRequest() {
        super();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "LeaveTypeRequest{" +
                "userId='" + userId + '\'' +
                '}';
    }
}
