package com.ess.api.leaverequest.requestBody;

public class LeaveSummaryRequest {

    private String userId;

    public LeaveSummaryRequest() {
        super();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "LeaveSummaryRequest{" +
                "userId='" + userId + '\'' +
                '}';
    }
}
