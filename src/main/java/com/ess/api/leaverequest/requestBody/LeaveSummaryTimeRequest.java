package com.ess.api.leaverequest.requestBody;

public class LeaveSummaryTimeRequest {

    private String userId;
    private String leaveType;
    private Integer leaveHour;
    private Integer leaveMinute;
    private String lastLeaveRequest;

    public LeaveSummaryTimeRequest() {
        super();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getLeaveType() {
        return leaveType;
    }

    public void setLeaveType(String leaveType) {
        this.leaveType = leaveType;
    }

    public Integer getLeaveHour() {
        return leaveHour;
    }

    public void setLeaveHour(Integer leaveHour) {
        this.leaveHour = leaveHour;
    }

    public Integer getLeaveMinute() {
        return leaveMinute;
    }

    public void setLeaveMinute(Integer leaveMinute) {
        this.leaveMinute = leaveMinute;
    }

    public String getLastLeaveRequest() {
        return lastLeaveRequest;
    }

    public void setLastLeaveRequest(String lastLeaveRequest) {
        this.lastLeaveRequest = lastLeaveRequest;
    }

    @Override
    public String toString() {
        return "LeaveSummaryTimeRequest{" +
                "userId='" + userId + '\'' +
                ", leaveType='" + leaveType + '\'' +
                ", leaveHour=" + leaveHour +
                ", leaveMinute=" + leaveMinute +
                ", lastLeaveRequest='" + lastLeaveRequest + '\'' +
                '}';
    }
}
