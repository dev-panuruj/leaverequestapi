package com.ess.api.leaverequest.requestBody;

public class LeaveRequest {

    private String username;

    public LeaveRequest() {
        super();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return "LeaveRequest{" +
                "username='" + username + '\'' +
                '}';
    }
}
