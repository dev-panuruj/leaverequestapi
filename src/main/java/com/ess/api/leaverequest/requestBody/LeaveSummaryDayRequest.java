package com.ess.api.leaverequest.requestBody;

public class LeaveSummaryDayRequest {

    private String userId;
    private String leaveType;
    private Integer leaveDay;
    private String lastLeaveRequest;

    public LeaveSummaryDayRequest() {
        super();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getLeaveType() {
        return leaveType;
    }

    public void setLeaveType(String leaveType) {
        this.leaveType = leaveType;
    }

    public Integer getLeaveDay() {
        return leaveDay;
    }

    public void setLeaveDay(Integer leaveDay) {
        this.leaveDay = leaveDay;
    }

    public String getLastLeaveRequest() {
        return lastLeaveRequest;
    }

    public void setLastLeaveRequest(String lastLeaveRequest) {
        this.lastLeaveRequest = lastLeaveRequest;
    }

    @Override
    public String toString() {
        return "LeaveSummaryDayRequest{" +
                "userId='" + userId + '\'' +
                ", leaveType='" + leaveType + '\'' +
                ", leaveDay=" + leaveDay +
                ", lastLeaveRequest='" + lastLeaveRequest + '\'' +
                '}';
    }
}
