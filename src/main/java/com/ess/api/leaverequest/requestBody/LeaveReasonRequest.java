package com.ess.api.leaverequest.requestBody;

public class LeaveReasonRequest {

    private String leaveType;

    public LeaveReasonRequest() {
        super();
    }

    public String getLeaveType() {
        return leaveType;
    }

    public void setLeaveType(String leaveType) {
        this.leaveType = leaveType;
    }

    @Override
    public String toString() {
        return "LeaveReasonRequest{" +
                "leaveType='" + leaveType + '\'' +
                '}';
    }
}
