package com.ess.api.leaverequest.requestBody;

public class UserLevelRequest {

    private String employeeId;

    public UserLevelRequest() {
        super();
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    @Override
    public String toString() {
        return "UserLevelRequest{" +
                "employeeId='" + employeeId + '\'' +
                '}';
    }
}
