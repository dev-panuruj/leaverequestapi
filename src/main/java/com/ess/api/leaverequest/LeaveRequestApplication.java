package com.ess.api.leaverequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LeaveRequestApplication {

    static Logger logger = LoggerFactory.getLogger(LeaveRequestApplication.class);

	public static void main(String[] args) {
	    SpringApplication.run(LeaveRequestApplication.class, args);
	    logger.info("Leave Request Api start...");
	}

}

